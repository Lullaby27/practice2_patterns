namespace SubscriptionSystem;

public class Facade<T> where T : SubscriptionFactory, new()
{
    private T factory;

    public Facade()
    {
        factory = new();
    }

    public SubscriptionInterface? CreateSubscription(SubscriptionParameters parameters) =>
        factory.MakeSubscription(parameters);

    public void ExpandSubscription(SubscriptionInterface subscription, int monthCount) => subscription.Extend(monthCount);
}