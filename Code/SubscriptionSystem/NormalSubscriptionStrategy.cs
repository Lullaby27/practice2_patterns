namespace SubscriptionSystem;

public class NormalSubscriptionStrategy : SubscriptionStrategy
{
    public override decimal GetPrice() => price;
}