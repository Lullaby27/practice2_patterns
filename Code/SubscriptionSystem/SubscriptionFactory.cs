namespace SubscriptionSystem;

public abstract class SubscriptionFactory
{
    public SubscriptionFactory()
    {
    }

    public abstract SubscriptionInterface? MakeSubscription(SubscriptionParameters subscriptionParameters);
}