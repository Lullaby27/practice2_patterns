namespace SubscriptionSystem;

public class YandexPlusSubscriptionFactory : SubscriptionFactory
{
    public override SubscriptionInterface? MakeSubscription(SubscriptionParameters subscriptionParameters)
    {
        switch (subscriptionParameters)
        {
            case SubscriptionParameters.Normal:
                return new YandexPlusSubscription(new NormalSubscriptionStrategy());
            case SubscriptionParameters.Discount:
                return new YandexPlusSubscription(new DiscountSubscriptionStrategy());
            default:
                return null;
        }
    }
}