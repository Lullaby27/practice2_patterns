namespace SubscriptionSystem;

public class NetflixSubscriptionFactory : SubscriptionFactory
{
    public override SubscriptionInterface? MakeSubscription(SubscriptionParameters parameters)
    {
        switch (parameters)
        {
            case SubscriptionParameters.Normal:
                return new NetflixSubscription(new NormalSubscriptionStrategy());
            case SubscriptionParameters.Discount:
                return new NetflixSubscription(new DiscountSubscriptionStrategy());
            default:
                return null;
        }
    }
}