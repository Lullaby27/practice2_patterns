﻿using System;
using System.Runtime.InteropServices;

namespace SubscriptionSystem;

class Program
{
  public static void Main()
  {
    var tvController = new YandexPlusSubscriptionController();
    var defaultTvSubscription = tvController.GetSubscription(SubscriptionParameters.Normal);
    var withDiscountTvSubscription = tvController.GetSubscription(SubscriptionParameters.Discount);

    var intController = new NetflixSubscriptionController();
    var defaultIntSub = intController.GetSubscription(SubscriptionParameters.Normal);
    var withDiscountIntSub = intController.GetSubscription(SubscriptionParameters.Discount);

    Console.WriteLine(defaultTvSubscription?.GetInfo());
    Console.WriteLine(withDiscountTvSubscription?.GetInfo());
    Console.WriteLine(defaultIntSub?.GetInfo());
    Console.WriteLine(withDiscountIntSub?.GetInfo());

  }
}