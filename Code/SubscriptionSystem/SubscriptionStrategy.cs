namespace SubscriptionSystem;

public abstract class SubscriptionStrategy
{
    protected decimal price = 500;

    public SubscriptionStrategy()
    {
    }

    public abstract decimal GetPrice();
}