namespace SubscriptionSystem;

public class NetflixSubscription : SubscriptionInterface
{
    public SubscriptionStrategy Strategy { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public decimal Price { get; set; }
    public DateTime EndDate { get; set; }

    public NetflixSubscription(SubscriptionStrategy strategy)
    {
        Strategy = strategy;
        Title = "Netflix";
        Description = "�������� Netflix 1 �����";
        Price = Strategy.GetPrice();
        EndDate = DateTime.Now.AddMonths(1);
    }
    public void Extend(int monthCount) => EndDate = EndDate.AddMonths(monthCount);
    public string GetInfo() => $"{Title} - {Description} - {Price} - {EndDate}";
}