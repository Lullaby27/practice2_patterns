﻿namespace SubscriptionSystem;

public class NetflixSubscriptionController
{
    public NetflixSubscription? GetSubscription(SubscriptionParameters parameters) =>
        (NetflixSubscription) new Facade<NetflixSubscriptionFactory>().CreateSubscription(parameters);
}