using System.Runtime.CompilerServices;

namespace SubscriptionSystem;

public class DiscountSubscriptionStrategy : SubscriptionStrategy
{
    private int discountP = 25;
    public DiscountSubscriptionStrategy()
    {
    }

    public override decimal GetPrice() => price - (price / 100 * discountP);
}