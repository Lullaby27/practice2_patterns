﻿namespace SubscriptionSystem;

public class YandexPlusSubscriptionController
{
    public YandexPlusSubscription? GetSubscription(SubscriptionParameters parameters) =>
        (YandexPlusSubscription) new Facade<YandexPlusSubscriptionFactory>().CreateSubscription(parameters);
}