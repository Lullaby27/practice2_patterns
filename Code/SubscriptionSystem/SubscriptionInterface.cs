namespace SubscriptionSystem;

public interface SubscriptionInterface
{
    SubscriptionStrategy Strategy { get; set; }
    string Title { get; set; }
    string Description { get; set; }
    decimal Price { get; set; }
    DateTime EndDate { get; set; }
    void Extend(int monthCount);
    string GetInfo();
}